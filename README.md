# README #


### What is this project for? ###

* This is an easy to use asset for adjusting audio levels in your game's build.

### How do I get set up? ###

* Download repo .zip
* Drag EasyAudioPanel folder into your Unity project.
* Drag AudioPanel prefab from the prefab folder into your scene heirarchy.
* Create an Audio Source with an audio clip.
* Set the Output parameter on your audio source to the desired audio mixer channel.

* Adjust panel visuals as necessary for your project.


### Who do I talk to for help? ###

* Daniel Ackroyd