﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePanel : MonoBehaviour {

    public GameObject UIPanel;
    private bool toggle = true;

    public string keyName = ("space");

    private void Start()
    {
        ToggleUI();
    }

    public void ToggleUI()
    {
        toggle = !toggle;
        UIPanel.gameObject.SetActive(toggle);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(keyName))
        {
            ToggleUI();
        }
    }
}
