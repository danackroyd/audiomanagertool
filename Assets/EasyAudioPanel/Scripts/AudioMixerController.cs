﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class AudioMixerController : MonoBehaviour {

    private float volume;
    private int displayVolume;

    public string settingName = ("");
    public Text text;
    public AudioMixer masterMixer;
    public Slider slider;

    //((input - min) * 100) / (max - min)
    private int max = 0; //100% on the slider
    private int min = -80; //0% on the slider

	// Use this for initialization
	void Start () {
        slider = gameObject.GetComponent<Slider>();
        UpdateSlider();
    }

    //Gets the value on the mixer and updates the slider to match the volume setting.
    public void UpdateSlider()
    {
        masterMixer.GetFloat(settingName, out volume);
        displayVolume = (((int)volume - min) * 100) / (max - min);
        slider.value = displayVolume;
        text.text = displayVolume.ToString();
    }

    public void OnSliderChange(float val)
    {

        volume = (val * (max - min) / 100) + min;
        masterMixer.SetFloat(settingName, volume);

        displayVolume = (((int)volume - min) * 100) / (max - min);
        text.text = displayVolume.ToString();
    }
}
